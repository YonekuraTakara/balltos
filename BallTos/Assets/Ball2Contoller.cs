﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2Contoller : MonoBehaviour
{
    Rigidbody2D rigid2D;
    float BoundForce = 390.0f;

    GameObject Player1;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.rigid2D.AddForce(transform.up * this.BoundForce);

        this.Player1 = GameObject.Find("Player1");

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -10.0f)
        {
            Destroy(gameObject);
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseNCo2();
        }

        Vector2 b1 = transform.position;
        Vector2 b2 = this.Player1.transform.position;
        Vector2 bth = b1 - b2;
        float t = bth.magnitude;
        float h1 = 0.5f;
        float h2 = 1.0f;

        if (t < h1 + h2)
        {
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseDm2();
            Destroy(gameObject);
        }

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);

        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo2();

        
    }
}

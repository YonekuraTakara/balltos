﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameDirector : MonoBehaviour
{
    // Start is called before the first frame update

    public static int Co = 0;
    public static int Co2 = 0;
    public static int WinCo = 0;
    public static float time = 1800.0f;
   
    public GameObject BallCount = null;
    public GameObject Ball2Count = null;
    GameObject Timer;

    public static int P1 = 0;
    public static int P2 = 0;
    public static int Win = 0;
    public GameObject P1Gauge;
    public GameObject P2Gauge;

    void Start()
    {
        this.Timer = GameObject.Find("Timer");
        time = 30.0f;
        Co = 0;
        Co2 = 0;

        this.P1Gauge = GameObject.Find("P1Gauge");
        this.P2Gauge = GameObject.Find("P2Gauge");
        P1 = 0;
        P2 = 0;
        Win = 0;
    }

    
    public void DecreaseCo()
    {
        Co++;
    }

    public void DecreaseCo2()
    {
        Co2++;
    }

    public void DecreaseNCo1()
    {
        Co--;
    }

    public void DecreaseNCo2()
    {
        Co2--;
    }

    public void DecreaseDm1()
    {
        this.P1Gauge.GetComponent<Image>().fillAmount -= 0.05f;
        P1++;
        Co--;
    }

    public void DecreaseDm2()
    {
        this.P2Gauge.GetComponent<Image>().fillAmount -= 0.05f;
        P2++;
        Co2--;
    }

    void Update()
    {
        //矢のカウント
        Text arrowText = this.BallCount.GetComponent<Text>();
        arrowText.text = Co + "個";

        //矢のカウント2
        Text arrow2Text = this.Ball2Count.GetComponent<Text>();
        arrow2Text.text = Co2 + "個";

        //タイマー
        time -= Time.deltaTime;
        this.Timer.GetComponent<Image>().fillAmount -= 0.00056f;

        //ゲームオーバー判定
        if (P1 >= 20)
        {
            SceneManager.LoadScene("GameOverScene");
            Win = 1;
        }

        if (P2 >= 20)
        {
            SceneManager.LoadScene("GameOverScene");
            Win = 2;
        }

        if(time <= 0)
        {
            if(Co > Co2)
            {
                Debug.Log("１の勝ち");
                SceneManager.LoadScene("GameOverScene");
                WinCo = Co;
                Win = 1;
            }
            else if(Co < Co2)
            {
                Debug.Log("２の勝ち");
                SceneManager.LoadScene("GameOverScene");
                WinCo = Co2;
                Win = 2;
            }
            else if (Co == Co2)
            {
                SceneManager.LoadScene("GameOverScene");
                Win = 3;
            }

        }
    }
}

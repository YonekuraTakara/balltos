﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody2D rigid2D;
    float BoundForce = 390.0f;

    GameObject Player2;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.rigid2D.AddForce(transform.up * this.BoundForce);

        this.Player2 = GameObject.Find("Player2");

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -10.0f)
        {
            Destroy(gameObject);
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseNCo1();
        }

        Vector2 p1 = transform.position;
        Vector2 p2 = this.Player2.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;

        if (d < r1 + r2)
        {
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseDm1();
            Destroy(gameObject);
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);

        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo();

        //SceneManager.LoadScene("ClearScene");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basket2Contoller : MonoBehaviour
{
    GameObject player;
    float px = 0;

    // Start is called before the first frame update
    void Start()
    {
        px = Random.Range(-0.1f, -0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(px, 0, 0);

        if (transform.position.x > 11.0f)
        {
            Destroy(gameObject);
        }
        if (transform.position.x < -11.0f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2Generator : MonoBehaviour
{
    public GameObject Ball2Prefab;

    float delta = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float time = GameDirector.time;
        this.delta += Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            if(this.delta >= 0.5f)
            {
                if(time > 0)
                {
                    delta = delta - 0.5f;
                    GameObject ball = Instantiate(Ball2Prefab) as GameObject;
                    ball.transform.position = transform.position;
                    ball.transform.rotation = transform.rotation;
                }
            }
        }
    }
}

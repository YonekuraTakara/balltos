﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow2Controrar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int rotSpeed = 0;

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rotSpeed = 2;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rotSpeed = -2;
        }

        transform.Rotate(0, 0, rotSpeed);
    }

}

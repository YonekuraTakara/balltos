﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    public GameObject Basket2Prefab;
    float span = 1.0f;
    float delta1 = 0;
    float delta2 = 0;
    public static float px = 0;


    // Update is called once per frame
    void Update()
    {
        this.delta1 += Time.deltaTime;
        this.delta2 += Time.deltaTime;
        if (this.delta1 > this.span)
        {
            this.delta1 = 0;
            GameObject go = Instantiate(BasketPrefab) as GameObject;
            go.transform.position = new Vector3(-10, -3, 0);
        }

        if (this.delta2 > this.span)
        {
            this.delta2 = 0;
            GameObject go = Instantiate(Basket2Prefab) as GameObject;
            go.transform.position = new Vector3(10, -3, 0);
        }
    }
}

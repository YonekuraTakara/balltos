﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverDirector : MonoBehaviour
{
    public GameObject Player1;
    public GameObject Player2;
    public static int Win = 0;
    public static int WinCo = 0;




    // Start is called before the first frame update
    void Start()
    {
        Win = GameDirector.Win;
        WinCo = GameOverDirector.WinCo;

        if (Win == 1)
        {
            GameObject go1 = Instantiate(Player1) as GameObject;
            go1.transform.position = new Vector3(-7, 0, 0);
        }

        if (Win == 2)
        {
            GameObject go2 = Instantiate(Player2) as GameObject;
            go2.transform.position = new Vector3(7, 0, 0);
        }

        if(Win == 3)
        {
            GameObject go1 = Instantiate(Player1) as GameObject;
            go1.transform.position = new Vector3(-7, 0, 0);

            GameObject go2 = Instantiate(Player2) as GameObject;
            go2.transform.position = new Vector3(7, 0, 0);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("GameScene");
        }
    }
}

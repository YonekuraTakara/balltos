﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject BallPrefab;

    float delta = 0;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float time = GameDirector.time;
        this.delta += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (this.delta >= 0.5f)
            {
                if (time > 0)
                {
                    delta = delta - 0.5f;
                    GameObject ball = Instantiate(BallPrefab) as GameObject;
                    ball.transform.position = transform.position;
                    ball.transform.rotation = transform.rotation;
                }
            }
        }
    }
}
